const homeScreenEvents = {
    'Buy Button': 'ev-buy',
};

const navigationEvents = {
    'Explore Link': 'ev-explore',
    'Shopping Cart': 'ev-shopping-cart',
};

const footerEvents = {
    'Chat Now': 'ev-chat',
};

const shoppingCart = {
  'Checkout Button': 'ev-checkout'
  'Confirm Charge Button': 'ev-confirm-charge'
};

const events = {
    'Shopping-Site': {
        'Home Screen': homeScreenEvents,
        'Navigation': navigationEvents,
        'Footer': footerEvents,
        'Shopping Cart': shoppingCart;
    },
};

const iterKeys = (object, func) => Object.keys(object).forEach(func);

iterKeys(events, category => iterKeys(events[category], label => iterKeys(events[category][label], event => {
    const className = events[category][label][event];
    $(`.${className}`).on('click', function(event) {
        analytics.track(event, {
            category,
            label,
        });
        if (label === 'Shopping Cart') {
          slackChannelAlert(label);
        };
    });
})));
